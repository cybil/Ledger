package xyz.ledger.util

enum class Currency constructor(val symbol: String) {
    GBP("£"),
    EUR("€"),
    USD("$");
}
