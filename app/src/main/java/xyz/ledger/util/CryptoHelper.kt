package xyz.ledger.util

import xyz.ledger.data.CurrencyPairInfo
import xyz.ledger.data.Ticker

fun getCryptoWithMode(ticker: Ticker, mode: String): CurrencyPairInfo {
    return ticker.data.firstOrNull { mode == it.pair }
            ?: ticker.data[0]
}
