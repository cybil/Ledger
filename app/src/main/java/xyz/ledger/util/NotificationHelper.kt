package xyz.ledger.util

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import xyz.ledger.R
import xyz.ledger.data.Ticker
import xyz.ledger.data.local.Cache
import java.math.BigDecimal


fun showNotification(context: Context, title: String, message: String, id: Int) {
    val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val name = context.getString(R.string.channel_name)// The user-visible name of the channel.
        val importance = NotificationManager.IMPORTANCE_HIGH
        val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
        mNotificationManager.createNotificationChannel(mChannel)
        val notificationBuilder = Notification.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(message)
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
        val startAppIntent = Intent(context, LauncherActivity::class.java)
        val contentIntent = PendingIntent.getActivity(context, 0, startAppIntent, 0)
        notificationBuilder.setContentIntent(contentIntent)

        NotificationManagerCompat.from(context).notify(id, notificationBuilder.build())
    } else {
        val notificationBuilder = Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
        val startAppIntent = Intent(context, LauncherActivity::class.java)
        val contentIntent = PendingIntent.getActivity(context, 0, startAppIntent, 0)
        notificationBuilder.setContentIntent(contentIntent)

        NotificationManagerCompat.from(context).notify(id, notificationBuilder.build())
    }
}

val CHANNEL_ID = "ticker"

fun checkLimitFilters(context: Context, ticker: Ticker, cache: Cache) {
    for (item in ticker.data) {
        for (stopLoss in cache.stopLosses) {
            if (stopLoss.pair == item.pair) {
                val low = stopLoss.low
                val high = stopLoss.high
                if (item.last <= BigDecimal(low) || item.last >= BigDecimal(high)) {
                    showNotification(context, item.pair, "limit_low: ".plus(low).plus("-").plus("limit_high: ").plus(high).plus("\ncurrent: ").plus(item.last), item.pair.hashCode())
                }
            }
        }
    }
}
