package xyz.ledger.util

import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


class CexSignature(val signature: String, val nonce: String, val apiKey: String)

fun cexSignature(userName: String, apiKey: String, apiSecret: String): CexSignature {
    val nonce = Nonce.increment()
    val message = nonce + userName + apiKey
    var hmac: Mac? = null

    try {
        hmac = Mac.getInstance("HmacSHA256")
        val secret_key = SecretKeySpec((apiSecret).toByteArray(charset("UTF-8")), "HmacSHA256")
        hmac!!.init(secret_key)
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    } catch (e: InvalidKeyException) {
        e.printStackTrace()
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    }

    val signature = String.format("%X", BigInteger(1, hmac!!.doFinal(message.toByteArray())))

    return CexSignature(signature, nonce, apiKey)
}
