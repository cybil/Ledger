package xyz.ledger.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import xyz.ledger.R;

public class DialogHelper {

    public static void showDialog(Context context, @StringRes int titleId, String message, DialogInterface.OnClickListener onOK) {
        showDialog(context, titleId, message, false, null, null, onOK, null, null);
    }

    public static void showDialog(Context context, @StringRes int titleId, String message, boolean showCancelButton, DialogInterface.OnClickListener onOK) {
        showDialog(context, titleId, message, showCancelButton, null, null, onOK, null, null);
    }

    public static void showDialog(Context context, @StringRes int titleId, String message, boolean showCancelButton, String primaryButtonTitle, String secondaryButtonTitle, DialogInterface.OnClickListener onOK, DialogInterface.OnClickListener onSecondary, ArrayAdapter<String> listItemsAdapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, listItemsAdapter == null ? onOK : null)
                .setCancelable(onOK == null || showCancelButton);
        if (titleId > 0) {
            builder.setTitle(titleId);
        }
        if (message != null && !message.isEmpty() && listItemsAdapter == null) {
            builder.setMessage(message);
        }
        if (showCancelButton) {
            builder.setNegativeButton(android.R.string.cancel, null);
        }
        if (primaryButtonTitle != null) {
            builder.setPositiveButton(primaryButtonTitle, onOK);
        }
        if (secondaryButtonTitle != null) {
            builder.setNegativeButton(secondaryButtonTitle, onSecondary);
        }
        if (listItemsAdapter != null) {
            builder.setAdapter(listItemsAdapter, onOK);
        }
        builder.create().show();
    }

    public static void showErrorDialog(Context context, Throwable t) {
        showErrorDialog(context, t, null);
    }

    public static void showErrorDialog(Context context, Throwable t, DialogInterface.OnClickListener onOK) {
//        if (t instanceof ApiClient.NoNetworkException) {
//            showDialog(context, R.string.error_oops_title, R.string.error_no_network, onOK);
//        } else {
            showGeneralFailureDialog(context, onOK);
//        }
    }

    public static void showDialog(Context context, @StringRes int titleId, @StringRes int messageId, DialogInterface.OnClickListener onOK) {
        showDialog(context, titleId, context.getString(messageId), false, null, null, onOK, null, null);
    }

    public static void showGeneralFailureDialog(Context context, DialogInterface.OnClickListener onOK) {
        showDialog(context, R.string.error_oops_title, R.string.error_try_again, onOK);
    }

    public static void showDialog(Context context, @StringRes int titleId, @StringRes int messageId, boolean showCancelButton, DialogInterface.OnClickListener onOK) {
        showDialog(context, titleId, context.getString(messageId), showCancelButton, null, null, onOK, null, null);
    }

    public static void showGeneralFailureDialog(Context context) {
        showGeneralFailureDialog(context, null);
    }

    public static void showDialogWithList(Context context, @StringRes int titleId, @StringRes int messageId, DialogInterface.OnClickListener onItemSelected, ArrayAdapter<String> listItemsAdapter) {
        showDialog(context, titleId, context.getString(messageId), false, null, null, onItemSelected, null, listItemsAdapter);
    }

    public static void showDialogWithSecondaryButton(Context context, @StringRes int titleId, @StringRes int message, @StringRes int secondaryButtonTitleId, DialogInterface.OnClickListener onOk, DialogInterface.OnClickListener onSecondary) {
        showDialog(context, titleId, context.getString(message), false, null, context.getString(secondaryButtonTitleId), onOk, onSecondary, null);
    }

    public static void showDialogWithNeutralButton(Context context, @StringRes int titleId, String message, @StringRes int primaryButtonTitleId, String secondaryButtonTitleId, DialogInterface.OnClickListener onOk, DialogInterface.OnClickListener onSecondary) {
        showDialog(context, titleId, message, true, context.getString(primaryButtonTitleId), secondaryButtonTitleId, onOk, onSecondary, null);
    }
}
