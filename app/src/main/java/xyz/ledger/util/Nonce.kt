package xyz.ledger.util

class Nonce {
    companion object {
        var nonce: Long = (System.currentTimeMillis() / 1000)
        fun increment(): String {
            nonce = nonce.plus(1)
            return nonce.toString()
        }
    }
}