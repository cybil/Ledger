package xyz.ledger

import android.app.Application
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import xyz.ledger.dagger.AppComponent
import xyz.ledger.dagger.AppModule
import xyz.ledger.dagger.DaggerAppComponent
import xyz.ledger.data.Api
import xyz.ledger.data.EmptyPrivateRequest
import xyz.ledger.util.Nonce
import xyz.ledger.util.cexSignature
import xyz.ledger.widget.FontCache
import javax.inject.Inject

class App : Application() {
    @Inject
    lateinit var api: Api

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        FontCache.init(this)
        Nonce()
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
        appComponent.inject(this)

        val userId = "up108164103"
        val apiKey = "VtDD8eWHCsWWR0w0RPxju7wLT0"
        val secret = "FIb31LKlBV584nvqDaWfsYgaZE"

//        val userId = "up106711828"
//        val apiKey = "QNmPS642SCsB5S3lLhctVs7Cig"
//        val secret = "UFVbAzXSFRQEWxWyhHR7hm7teKg"

        api.orders(EmptyPrivateRequest.init(cexSignature(userId, apiKey, secret)))
                .subscribeOn(Schedulers.io())
                .subscribe({ r ->
                    Timber.d("success-->".plus(r))
                }, { t ->
                    Timber.e("exception-->".plus(t))
                })


        api.balance(EmptyPrivateRequest.init(cexSignature(userId, apiKey, secret)))
                .subscribeOn(Schedulers.io())
                .subscribe({ r ->
                    Timber.d("success-->".plus(r))
                }, { t ->
                    Timber.e("exception-->".plus(t))
                })
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}