package xyz.ledger.domain

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_ticker.*
import kotlinx.android.synthetic.main.content_ticker.*
import timber.log.Timber
import xyz.ledger.App
import xyz.ledger.R
import xyz.ledger.data.Ticker
import xyz.ledger.data.TickerPair
import xyz.ledger.data.TickerService
import xyz.ledger.data.local.Cache
import xyz.ledger.extensions.format
import xyz.ledger.util.getCryptoWithMode
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TickerActivity : AppCompatActivity() {
    @Inject
    lateinit var cache: Cache

    private var currentMode: String = "BTC:USD"
    private var items: ArrayList<TickerPair> = arrayListOf()
    private var tickerRecyclerAdapter: TickerRecyclerAdapter? = null
    private var disposables = CompositeDisposable()
    private var lastPrice: BigDecimal = BigDecimal.ZERO

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, TickerActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticker)
        setSupportActionBar(toolbar)
        App.appComponent.inject(this)

        initView()
        startTicker()
    }

    private fun initView() {
        supportActionBar?.title = currentMode
        setDialogAdapter()
        tickerRecyclerAdapter = TickerRecyclerAdapter(items, cache)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        layoutManager.stackFromEnd = true
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = tickerRecyclerAdapter
    }

    private fun setDialogAdapter() {
        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1)
        cache.pairs.forEach { pair -> arrayAdapter.add(pair) }
        val pairDialog = AlertDialog.Builder(this)
        pairDialog.setAdapter(arrayAdapter) { _, which ->
            currentMode = arrayAdapter.getItem(which)
            reset()
        }
        img_drop_down.setOnClickListener {
            pairDialog.show()
        }

        toolbar.setOnClickListener {
            pairDialog.show()
        }
    }

    private fun startTicker() {
        disposables.add(Observable.interval(0L, cache.interval, TimeUnit.SECONDS)
                .flatMap { TickerService.usd.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
                .subscribe({ data ->
                    if (lastPrice == getData(data, currentMode).pair.last) {
                        tickerRecyclerAdapter?.notifyItemChanged(items.size - 1, "no-change")
                    } else {
                        items.add(getData(data, currentMode))
                        tickerRecyclerAdapter?.notifyItemInserted(items.size)
                        recycler_view.scrollToPosition(items.size - 1)
                    }
                    lastPrice = getData(data, currentMode).pair.last
                }, { t ->
                    Timber.e(t)
                }))
    }

    private fun getData(ticker: Ticker, mode: String): TickerPair {
        val pairInfo = getCryptoWithMode(ticker, mode)
        return TickerPair(pairInfo, System.currentTimeMillis())
    }

    private fun reset() {
        supportActionBar?.title = currentMode
        items.clear()
        lastPrice = BigDecimal.ZERO
        tickerRecyclerAdapter?.lastPrice = "0"
        tickerRecyclerAdapter?.seconds = 0
        tickerRecyclerAdapter?.notifyDataSetChanged()
    }

    private class TickerRecyclerAdapter(private var items: List<TickerPair>, private var cache: Cache) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        var lastPrice = "0"
        var seconds = 0
        var disposables = CompositeDisposable()

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
            (holder as VHItem).lastPrice.text = lastPrice
            lastPrice = items[position].pair.last.format(4)
            holder.lastPrice.text = lastPrice
            holder.seconds.text = when (items[position].timestamp == 0L) {
                true -> ""
                else -> {
                    items[position].timestamp.toString().plus(" sec")
                }
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int, payloads: MutableList<Any>?) {
            if (payloads != null && !payloads.isEmpty()) {
                (holder as VHItem).lastPrice.text = lastPrice
                lastPrice = items[position].pair.last.format(4)
                seconds += cache.interval.toInt()
                startCounter(holder, seconds, position)
            } else {
                seconds = 0
                items[position].timestamp = 0
                onBindViewHolder(holder, position)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.ticker_item, parent, false)
            return TickerRecyclerAdapter.VHItem(view)
        }

        override fun getItemCount() = items.size

        class VHItem constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var lastPrice: AppCompatTextView = itemView.findViewById(R.id.last_price)
            var seconds: AppCompatTextView = itemView.findViewById(R.id.seconds)
        }

        private fun startCounter(holder: VHItem, seconds: Int, position: Int) {
            disposables.add(Observable.intervalRange(0L, cache.interval, 0L, 1L, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext { interval ->
                        items[position].timestamp = interval + seconds
                        holder.seconds.text = (interval + seconds).toString().plus(" sec")
                    }.subscribe())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
        tickerRecyclerAdapter?.disposables?.clear()
    }
}
