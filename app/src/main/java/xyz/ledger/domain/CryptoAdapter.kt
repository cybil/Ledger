package xyz.ledger.domain

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import xyz.ledger.R
import xyz.ledger.data.CurrencyPairInfo
import xyz.ledger.extensions.format
import kotlin.jvm.internal.Intrinsics.checkNotNull

class CryptoAdapter(private var cryptoList: MutableList<CurrencyPairInfo>, var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val startSettings: PublishSubject<Int> = PublishSubject.create()

    init {
        setData(cryptoList)

        startSettings.subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread())
                .doOnNext { position ->
                    SettingsActivity.start(context, cryptoList[position].pair)
                }.subscribe()
    }

    fun setData(crypto: MutableList<CurrencyPairInfo>) {
        checkNotNull(crypto)
        this.cryptoList = crypto
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            ITEM -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.crypto_item, parent, false)
                return VHItem(view, startSettings)
            }
            HEADER -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.crypto_header, parent, false)
                return VHHeader(view)
            }
            FOOTER -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.crypto_footer, parent, false)
                return VHFooter(view)
            }
            else -> throw RuntimeException(
                    parent.context.getString(R.string.exception_no_view_type_found))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == ITEM) {
            val crypto = holder as VHItem
            val finalPosition = position - 1
            crypto.pair.text = cryptoList[finalPosition].pair
            crypto.lastPrice.text = cryptoList[finalPosition].last.format(2)
            crypto.high.text = crypto.pair.context.getString(R.string.high_price).plus(cryptoList[finalPosition].high.format())
            crypto.low.text = crypto.pair.context.getString(R.string.low_price).plus(cryptoList[finalPosition].low.format())
            crypto.ask.text = crypto.pair.context.getString(R.string.ask_price).plus(cryptoList[finalPosition].ask.format())
            crypto.bid.text = crypto.pair.context.getString(R.string.bid_price).plus(cryptoList[finalPosition].bid.format())
        }
    }

    override fun getItemCount(): Int {
        return cryptoList.size + 2
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> HEADER
            cryptoList.size + 1 -> FOOTER
            else -> ITEM
        }
    }

    private class VHItem constructor(itemView: View, val startSettings: PublishSubject<Int>) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var pair: AppCompatTextView = itemView.findViewById(R.id.pair)
        var high: AppCompatTextView = itemView.findViewById(R.id.high)
        var low: AppCompatTextView = itemView.findViewById(R.id.low)
        var lastPrice: AppCompatTextView = itemView.findViewById(R.id.last_price)
        var ask: AppCompatTextView = itemView.findViewById(R.id.ask)
        var bid: AppCompatTextView = itemView.findViewById(R.id.bid)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            startSettings.onNext(adapterPosition - 1)
        }
    }

    private class VHHeader internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    private class VHFooter internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {

        private val HEADER = 1
        private val ITEM = 2
        private val FOOTER = 3
    }
}