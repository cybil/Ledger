package xyz.ledger.domain.dashboard

import android.databinding.ObservableField
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import xyz.ledger.data.Api
import xyz.ledger.data.CurrencyPairInfo
import xyz.ledger.data.Ticker
import xyz.ledger.data.TickerService
import xyz.ledger.data.local.Cache
import xyz.ledger.util.Currency
import javax.inject.Inject


class DashboardViewModel
@Inject constructor(private val cache: Cache, private val api: Api) {
    val currency: ObservableField<Currency> = ObservableField(Currency.USD)

    fun setCurrency(currency: Currency) {
        this.currency.set(currency)
    }

    fun tickerStream(): Observable<Ticker> {
        return TickerService.usd.observeOn(AndroidSchedulers.mainThread()).subscribeOn(AndroidSchedulers.mainThread())
//                .map { t -> t.data.filter { it.pair.contains(currency.get().name) } }
    }
}
