package xyz.ledger.domain.dashboard

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_dashboard.*
import xyz.ledger.App
import xyz.ledger.R
import xyz.ledger.data.CurrencyPairInfo
import xyz.ledger.data.TickerService
import xyz.ledger.domain.CryptoAdapter
import xyz.ledger.domain.IntervalActivity
import xyz.ledger.domain.TickerActivity
import xyz.ledger.domain.login.LoginActivity
import xyz.ledger.extensions.isServiceRunning
import xyz.ledger.util.DialogHelper
import javax.inject.Inject


class DashboardActivity : AppCompatActivity() {
    @Inject lateinit var viewModel: DashboardViewModel

    private var disposables = CompositeDisposable()
    private var serviceIntent: Intent? = null

    private var cryptoList = mutableListOf<CurrencyPairInfo>()
    private var recyclerAdapter = CryptoAdapter(cryptoList, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(action_bar)
        App.appComponent.inject(this)

        initView()

        startTicker()

        start()
    }

    fun start() {
        disposables.clear()
        disposables.add(viewModel.tickerStream()
                .doOnError { t -> DialogHelper.showErrorDialog(this, t) }
                .subscribe({ usd -> setValues(usd.data) }))
    }

    override fun onResume() {
        super.onResume()
        recyclerAdapter.notifyDataSetChanged()
    }

    private fun initView() {
        ticker_switch.isChecked = isServiceRunning(TickerService::class.java)
        ticker_switch.setOnClickListener {
            if (ticker_switch.isChecked) {
                startTicker()
            } else {
                stopTicker()
            }
        }


        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = recyclerAdapter
    }

    private fun startTicker() {
        serviceIntent = Intent(this, TickerService::class.java)
        startService(serviceIntent)
    }

    private fun stopTicker() {
        stopService(serviceIntent)
    }

    private fun setValues(usd: List<CurrencyPairInfo>) {
        cryptoList.clear()
        cryptoList.addAll(usd)
        recyclerAdapter.setData(cryptoList)
        ticker_switch.isChecked = isServiceRunning(TickerService::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_ticker -> {
                TickerActivity.start(this)
                true
            }
            R.id.menu_ticker_update_interval -> {
                startActivity(Intent(this, IntervalActivity::class.java))
                true
            }
            R.id.menu_login -> {
                LoginActivity.start(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}
