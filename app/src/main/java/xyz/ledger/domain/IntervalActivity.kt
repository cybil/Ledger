package xyz.ledger.domain

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import xyz.ledger.App
import xyz.ledger.R
import xyz.ledger.data.Interval
import xyz.ledger.data.TickerService
import xyz.ledger.data.local.Cache
import xyz.ledger.data.local.StopLossDao
import javax.inject.Inject

class IntervalActivity : Activity() {
    @Inject
    lateinit var cache: Cache
    @Inject
    lateinit var dao: StopLossDao
    private var intervalEditText: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_interval)
        App.appComponent.inject(this)
        intervalEditText = findViewById(R.id.edit_text_interval)
        intervalEditText!!.setText(cache.interval.toString(), TextView.BufferType.EDITABLE)
        findViewById<Button>(R.id.save_interval).setOnClickListener {
            val interval = intervalEditText?.text?.toString()?.toLong()
            if (interval != null) {
                dao.insertInterval(Interval(1, interval))
                TickerService.updateInterval.onNext(interval)
                cache.interval = interval
                finish()
            } else {
                Snackbar.make(intervalEditText!!, "Try another, Intervals must be a decimal and in seconds", Snackbar.LENGTH_SHORT).show()
            }
        }
    }
}
