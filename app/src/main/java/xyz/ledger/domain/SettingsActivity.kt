package xyz.ledger.domain

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_settings.*
import xyz.ledger.App
import xyz.ledger.R
import xyz.ledger.data.StopLossPairs
import xyz.ledger.data.local.Cache
import xyz.ledger.data.local.StopLossDao
import javax.inject.Inject


class SettingsActivity : AppCompatActivity() {
    @Inject lateinit var cache: Cache
    @Inject lateinit var dao: StopLossDao

    private var currentMode: String = "BTC:USD"

    companion object {
        fun start(context: Context, mode: String) {
            val intent = Intent(context, SettingsActivity::class.java)
            intent.putExtra(MODE_PRAM, mode)
            context.startActivity(intent)
        }

        const val MODE_PRAM = "settings_mode"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        App.appComponent.inject(this)

        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        val pair = intent.getSerializableExtra(MODE_PRAM) as? String
        currentMode = pair ?: "BTC:USD"
        header.text = currentMode
        initView()
    }

    private fun initView() {
        edt_high.clearFocus()
        for (stopLoss in cache.stopLosses) {
            if (stopLoss.pair == currentMode) {
                val low = stopLoss.low
                val high = stopLoss.high
                setLimits(stopLoss, low, high)
            }
        }

        btn_save.setOnClickListener {
            dao.insertStopLoss(StopLossPairs(currentMode, true, edt_low.text.toString(), edt_high.text.toString()))
            cache.refresh()
            finish()
        }
    }

    private fun setLimits(stopLossPairs: StopLossPairs?, low: String?, high: String?) {
        pair.text = stopLossPairs?.pair
        edt_low.setText(low, TextView.BufferType.EDITABLE)
        edt_high.setText(high, TextView.BufferType.EDITABLE)
    }

    fun goBack(@Suppress("UNUSED_PARAMETER") v: View) {
        finish()
    }
}
