package xyz.ledger.widget

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import xyz.ledger.R
import java.math.BigDecimal


class PriceIndicatorTextView : SolomonTextViewBold {
    private var oldValue: BigDecimal? = null
    private var holdChange: Boolean = true

    constructor(context: Context) : super(context) {
        oldValue = BigDecimal.ZERO
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        oldValue = BigDecimal.ZERO
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PriceIndicatorTextView)
        holdChange = typedArray.getBoolean(R.styleable.PriceIndicatorTextView_holdChange, false)
        typedArray.recycle()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        oldValue = BigDecimal.ZERO
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PriceIndicatorTextView)
        holdChange = typedArray.getBoolean(R.styleable.PriceIndicatorTextView_holdChange, false)
        typedArray.recycle()
    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
        if (text != null && oldValue != null) {
            var newValue = oldValue
            try {
                newValue = BigDecimal(text.toString())
            } catch (e: NumberFormatException) {

            }
            when {
                oldValue!! < newValue -> setTextColor(ContextCompat.getColor(this.context, android.R.color.holo_green_light))
                oldValue!! > newValue -> setTextColor(ContextCompat.getColor(this.context, android.R.color.holo_red_light))
                !holdChange -> setTextColor(ContextCompat.getColor(this.context, android.R.color.holo_orange_light))
            }
            oldValue = newValue
        }
    }
}
