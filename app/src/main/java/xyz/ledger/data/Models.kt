package xyz.ledger.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.graphics.drawable.Drawable
import xyz.ledger.util.CexSignature
import java.math.BigDecimal

data class Ticker(val e: String, val ok: String, val data: List<CurrencyPairInfo>)

data class CurrencyPairInfo(val timestamp: String, @PrimaryKey val pair: String, val low: BigDecimal, val high: BigDecimal, val last: BigDecimal, val volume: BigDecimal, val volume30d: BigDecimal, val bid: BigDecimal, val ask: BigDecimal)

data class TickerPair(val pair: CurrencyPairInfo, var timestamp: Long)

@Entity(tableName = "StopLossPairs") data class StopLossPairs(@PrimaryKey var pair: String, var enabled: Boolean, var low: String, var high: String) {
}

@Entity(tableName = "Interval") data class Interval(@PrimaryKey var id: Int, var interval: Long)

//CEX.IO (Private api request)

data class EmptyPrivateRequest(val key: String?, val signature: String?, val nonce: String?) {
    companion object {
        fun init(signature: CexSignature): EmptyPrivateRequest {
            return EmptyPrivateRequest(signature.apiKey, signature.signature, signature.nonce)
        }
    }
}