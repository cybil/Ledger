package xyz.ledger.data.local

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import xyz.ledger.dagger.ApplicationScope
import xyz.ledger.data.StopLossPairs
import xyz.ledger.data.TickerService
import javax.inject.Inject

@ApplicationScope
class Cache @Inject constructor(private var dao: StopLossDao) {
    private val disposables = CompositeDisposable()
    var stopLosses: MutableList<StopLossPairs> = mutableListOf()
    val pairs: MutableList<String> = mutableListOf()
    var interval = 3L

    init {
        getStopLosses()
        getPairs()
    }

    private fun getStopLosses() {
        dao.stopLoss.subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ stopLoss ->
                    Timber.d("cache-stop-loss".plus(stopLoss.size.toString()))
                    processStopLoss(stopLoss)
                }, { t ->
                    Timber.e(t.message)
                })

        dao.interval().subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ updateInterval ->
                    Timber.d("cache-interval".plus(updateInterval.interval))
                    interval = updateInterval.interval
                }, { t ->
                    Timber.e(t.message)
                })
    }

    private fun getPairs() {
        disposables.add(TickerService.usd.subscribe { usd ->
            usd.data.mapTo(pairs) { it.pair }
            disposables.clear()
        })
    }

    private fun processStopLoss(stopLoss: List<StopLossPairs>) {
        stopLosses.clear()
        stopLosses.addAll(stopLoss)
    }

    fun refresh() {
        getStopLosses()
    }
}