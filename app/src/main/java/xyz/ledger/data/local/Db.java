package xyz.ledger.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import xyz.ledger.data.Interval;
import xyz.ledger.data.StopLossPairs;

@Database(entities = {StopLossPairs.class, Interval.class}, version = 2)
public abstract class Db extends RoomDatabase {

    abstract public StopLossDao userDao();
}
