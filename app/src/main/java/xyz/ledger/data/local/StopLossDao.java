package xyz.ledger.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Single;
import xyz.ledger.data.Interval;
import xyz.ledger.data.StopLossPairs;

@Dao
public interface StopLossDao {
    @Query("SELECT * FROM StopLossPairs")
    Single<List<StopLossPairs>> getStopLoss();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStopLoss(StopLossPairs pair);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInterval(Interval interval);

    @Query("SELECT * FROM Interval LIMIT 1")
    Single<Interval> interval();

    @Query("DELETE FROM StopLossPairs")
    void clearStoploss();
}

