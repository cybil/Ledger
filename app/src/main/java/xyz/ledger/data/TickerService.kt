package xyz.ledger.data

import android.app.Service
import android.content.Intent
import android.os.IBinder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import xyz.ledger.App
import xyz.ledger.data.local.Cache
import xyz.ledger.util.checkLimitFilters
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TickerService : Service() {
    @Inject
    lateinit var api: Api
    @Inject
    lateinit var cache: Cache

    private var disposables = CompositeDisposable()
    private var interval = 3L

    companion object {
        var usd: PublishSubject<Ticker> = PublishSubject.create()
        var updateInterval: PublishSubject<Long> = PublishSubject.create()
    }

    override fun onCreate() {
        super.onCreate()
        App.appComponent.inject(this)
        interval = cache.interval
        start(interval)

        disposables.add(updateInterval
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { interval ->
                    disposables.clear()
                    start(interval)
                })
    }

    private fun start(interval: Long) {
        disposables.add(Observable.interval(0L, interval, TimeUnit.SECONDS)
                .flatMap { zipAll().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
                .subscribe({ data ->
                    usd.onNext(data)
                    checkLimitFilters(this, data, cache)
                }, { t ->
                    Timber.e(t)
                }))
    }

    fun zipAll(): Observable<Ticker> {
        return Observable.zip(api.usd().toObservable(), api.eur().toObservable(), api.gbp().toObservable(), Function3 { u, e, g ->
            val all = u.data + e.data + g.data
            Ticker("", "ok", all)
        })
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int) = START_STICKY

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}