package xyz.ledger.data

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {
    @GET("api/tickers/USD")
    fun usd(): Single<Ticker>

    @GET("api/tickers/GBP")
    fun gbp(): Single<Ticker>

    @GET("api/tickers/EUR")
    fun eur(): Single<Ticker>

    @POST("api/open_orders/")
    fun orders(@Body request: EmptyPrivateRequest): Single<Any>

    @POST("api/balance/")
    fun balance(@Body request: EmptyPrivateRequest): Single<Any>
}