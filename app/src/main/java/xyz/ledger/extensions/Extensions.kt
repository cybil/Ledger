package xyz.ledger.extensions

import android.app.ActivityManager
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import java.math.BigDecimal
import java.util.*


fun BigDecimal.format(scale: Int = 4): String {
    return String.format(Locale.getDefault(), round(this, scale, false).toString(), 4, false)
}

fun AppCompatActivity.isServiceRunning(serviceClass: Class<*>): Boolean {
    val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    return manager.getRunningServices(Integer.MAX_VALUE).any { serviceClass.name == it.service.className }
}

private fun round(d: BigDecimal, scale: Int, roundUp: Boolean): BigDecimal {
    val mode = if (roundUp) BigDecimal.ROUND_UP else BigDecimal.ROUND_DOWN
    return d.setScale(scale, mode)
}

fun AppCompatActivity.addFragment(fragment: Fragment, container: Int, inAnim: Int, outAnim: Int, popInAnim: Int, popOutAnim: Int) {
    supportFragmentManager.beginTransaction().setCustomAnimations(inAnim, outAnim, popInAnim, popOutAnim).addToBackStack(null).replace(container, fragment, fragment::javaClass.name).commit()
}

fun AppCompatActivity.addFragment(fragment: Fragment, container: Int) {
    supportFragmentManager.beginTransaction().addToBackStack(null).replace(container, fragment, fragment::javaClass.name).commit()
}