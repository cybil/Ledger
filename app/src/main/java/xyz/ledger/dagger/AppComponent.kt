package xyz.ledger.dagger

import dagger.Component
import xyz.ledger.App
import xyz.ledger.data.Api
import xyz.ledger.data.TickerService
import xyz.ledger.data.local.Cache
import xyz.ledger.domain.*
import xyz.ledger.domain.dashboard.DashboardActivity

@ApplicationScope
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, DbModule::class))
interface AppComponent {
    fun provideApi(): Api
    fun provideCache(): Cache
    fun inject(app: App)
    fun inject(service: TickerService)
    fun inject(activity: DashboardActivity)
    fun inject(activity: SettingsActivity)
    fun inject(activity: TickerActivity)
    fun inject(activity: IntervalActivity)
}
