package xyz.ledger.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import xyz.ledger.App
import xyz.ledger.persistence.PreferencesServiceImpl


@Module
class AppModule(val app: App) {

    @Provides
    @ApplicationScope
    fun provideContext(): Context = app.applicationContext

    @Provides
    @ApplicationScope
    fun provideApp(): App = app

    @Provides
    @ApplicationScope
    fun providePreferencesService(app: App) = PreferencesServiceImpl(app)
}
