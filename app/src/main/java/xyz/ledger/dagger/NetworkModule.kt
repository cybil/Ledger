package xyz.ledger.dagger

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import xyz.ledger.App
import xyz.ledger.Constant
import xyz.ledger.data.Api
import java.io.File
import java.util.concurrent.TimeUnit
import android.net.NetworkInfo
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager


@Module(includes = arrayOf(AppModule::class))
class NetworkModule {

    @Provides
    @ApplicationScope
    internal fun cacheFile(app: App): File {
        return File(app.applicationContext.cacheDir, "okhttp_cache")
    }

    @Provides
    @ApplicationScope
    internal fun cache(cacheFile: File): Cache {
        return Cache(cacheFile, 10 * 1000 * 1000) // 10MB
    }

    @Provides
    @ApplicationScope
    internal fun loggingInterceptor(): HttpLoggingInterceptor {
        val interceptor: HttpLoggingInterceptor
        interceptor = HttpLoggingInterceptor { message -> Timber.i(message) }
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    @ApplicationScope
    internal fun okHttpClient(
            loggingInterceptor: HttpLoggingInterceptor, cache: Cache): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(loggingInterceptor)
                .addInterceptor({ chain ->
                    val builder = chain.request().newBuilder()
                    val charset = "UTF-8"

                    builder.header("Accept", "application/json")
                            .header("User-Agent", "Cex.io Android client")
                            .header("Accept-Charset", charset)
                            .header("Content-Type", "application/json")
                            .header("Charset", charset)
                    chain.proceed(builder.build())
                })
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .cache(cache)
                .build()
    }


    @Provides
    @ApplicationScope
    internal fun api(tickerRetrofit: Retrofit): Api {
        return tickerRetrofit.create<Api>(Api::class.java)
    }


    @Provides
    @ApplicationScope
    internal fun retrofit(
            okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constant.CEX_IO_BASE_URL)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @ApplicationScope
    internal fun gson(): Gson {
        return GsonBuilder().create()
    }
}
