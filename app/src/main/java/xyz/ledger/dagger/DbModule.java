package xyz.ledger.dagger;

import android.arch.persistence.room.Room;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import xyz.ledger.dagger.AppModule;
import xyz.ledger.dagger.ApplicationScope;
import xyz.ledger.data.local.Db;
import xyz.ledger.data.local.StopLossDao;

@Module(includes = AppModule.class)
public class DbModule {

    @ApplicationScope
    @Provides
    Db provideDb(Context context) {
        return Room.databaseBuilder(context, Db.class, "ledger.db").allowMainThreadQueries().build();
    }

    @ApplicationScope
    @Provides
    StopLossDao provideDao(Db db) {
        return db.userDao();
    }
}
