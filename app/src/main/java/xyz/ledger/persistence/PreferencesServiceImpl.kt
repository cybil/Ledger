package xyz.ledger.persistence

import android.content.Context
import android.content.SharedPreferences
import org.jetbrains.annotations.Nullable
import xyz.ledger.App
import xyz.ledger.dagger.ApplicationScope
import javax.inject.Inject


@ApplicationScope
class PreferencesServiceImpl @Inject constructor(app: App) : PreferencesService {
    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences = app.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
    }

    override fun getBool(key: PreferencesService.Key): Boolean {
        return sharedPreferences.getBoolean(key.toString(), false)
    }

    override fun setBool(key: PreferencesService.Key, value: Boolean) {
        sharedPreferences.edit().putBoolean(key.toString(), value).apply()
    }

    override fun getString(key: PreferencesService.Key, @Nullable defaultString: String): String {
        return sharedPreferences.getString(key.name, defaultString)
    }

    override fun setString(key: PreferencesService.Key, value: String) {
        sharedPreferences.edit().putString(key.name, value).apply()
    }

    override fun getLong(key: PreferencesService.Key, defaultLong: Long?): Long? {
        return sharedPreferences.getLong(key.name, defaultLong!!)
    }

    override fun setLong(key: PreferencesService.Key, value: Long?) {
        sharedPreferences.edit().putLong(key.name, value!!).apply()
    }

    override fun getInt(key: PreferencesService.Key, defaultInt: Int?): Int? {
        return sharedPreferences.getInt(key.name, defaultInt!!)
    }

    override fun setInt(key: PreferencesService.Key, value: Int?) {
        sharedPreferences.edit().putInt(key.name, value!!).apply()
    }

    companion object {
        private val PREFS_FILE = "preferences"
    }
}
