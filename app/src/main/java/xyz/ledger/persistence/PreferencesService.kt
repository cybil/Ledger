package xyz.ledger.persistence

import org.jetbrains.annotations.Nullable
import java.io.Serializable

interface PreferencesService : Serializable {
    fun getBool(key: Key): Boolean

    fun setBool(key: Key, value: Boolean)

    fun getString(key: Key, @Nullable defaultString: String): String

    fun setString(key: Key, value: String)

    fun getLong(key: Key, @Nullable defaultLong: Long?): Long?

    fun setLong(key: Key, value: Long?)

    fun getInt(key: Key, @Nullable defaultInt: Int?): Int?

    fun setInt(key: Key, value: Int?)

    enum class Key

}
